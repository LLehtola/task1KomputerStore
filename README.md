# task1KomputerStore

This project involves creating a simple web application with HTML, CSS and Javascript.

All HTML, CSS and Javascript files have been included separately and the naming of variables generally explains what is going on.

The application has 3 functionalities:

- Bank that has a bank balance and loan variables. There are buttons for applying for a loan of 2*(bank balance) or for repaying the loan from pay balance.

- Work that has a pay balance. There are buttons for incrementing this by 100 and for transferring pay balance to bank balance.

- Laptop that has a dropdown menu of laptop choices and it displays the selected choice on the right hand side once user selects laptop with select laptop button. There is also a button for buing the selected laptop - this deducts the price from bank balance and displays message that the purchase was successful.
