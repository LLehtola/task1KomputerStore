
let payBalance;
let bankBalance;
let loanBalance;
let hasALoan;
let laptopSelection;

payBalance = 0;
bankBalance = 0;
loanBalance = 0;
laptopSelection=0;

document.getElementById("bankbalance").innerHTML = "Balance "+bankBalance+ "kr";
document.getElementById("paybalance").innerHTML = "Pay "+payBalance+ "kr";
document.getElementById("loanbalance").innerHTML = "Outstanding loan "+loanBalance+ "kr";
document.getElementById("laptopselection").innerHTML = "Laptop details displayed here";
document.getElementById("laptopdetails").innerHTML = "Please select a laptop";
document.getElementById("laptopmessage").innerHTML = "";

function getALoan() {
  if(loanBalance<0){

  }else{
    loanBalance=-(bankBalance*2);
    bankBalance=bankBalance-loanBalance;
    document.getElementById("bankbalance").innerHTML = "Balance "+bankBalance+ "kr";
    document.getElementById("paybalance").innerHTML = "Pay "+payBalance+ "kr";
    document.getElementById("loanbalance").innerHTML = "Outstanding loan "+loanBalance+ "kr";
    hasALoan=1;
  }
  document.getElementById("laptopmessage").innerHTML = "";
}

function bank() {
  if(loanBalance<0){
    hasALoan=1;
  }else{
    hasALoan=0;
  }
  bankBalance=bankBalance+payBalance*(1-hasALoan*0.1)
  loanBalance=loanBalance-payBalance*(hasALoan*0.1)
  payBalance=0;
  document.getElementById("bankbalance").innerHTML = "Balance "+bankBalance+ "kr";
  document.getElementById("paybalance").innerHTML = "Pay "+payBalance+ "kr";
  document.getElementById("loanbalance").innerHTML = "Outstanding loan "+loanBalance+ "kr";
  document.getElementById("laptopmessage").innerHTML = "";
}

function work() {
  payBalance=payBalance+100;
  document.getElementById("bankbalance").innerHTML = "Balance "+bankBalance+ "kr";
  document.getElementById("paybalance").innerHTML = "Pay "+payBalance+ "kr";
  document.getElementById("loanbalance").innerHTML = "Outstanding loan "+loanBalance+ "kr";
  document.getElementById("laptopmessage").innerHTML = "";
}

function repayALoan() {
  if(loanBalance<0){
    loanBalance=loanBalance+payBalance;
    payBalance=0;
    hasALoan=1
    if(loanBalance>0){
      payBalance=loanBalance
      loanBalance=0;
      hasALoan=0;
    }
  }else{
    hasALoan=0;
  }
  document.getElementById("bankbalance").innerHTML = "Balance "+bankBalance+ "kr";
  document.getElementById("paybalance").innerHTML = "Pay "+payBalance+ "kr";
  document.getElementById("loanbalance").innerHTML = "Outstanding loan "+loanBalance+ "kr";
  document.getElementById("laptopmessage").innerHTML = "";
}

function selectLaptop() {
  laptopSelection = document.getElementById("option").value;
  if(laptopSelection==0){
    document.getElementById("laptopselection").innerHTML = "Laptop details displayed here";
    document.getElementById("laptopdetails").innerHTML = "Please select a laptop";
  }else if(laptopSelection==1){
    document.getElementById("laptopselection").innerHTML = "You selected Intol Celeroon";
    document.getElementById("laptopdetails").innerHTML = "This is a bad laptop and costs 100kr";
  }else if(laptopSelection==2){
    document.getElementById("laptopselection").innerHTML = "You selected ABM Athlete";
    document.getElementById("laptopdetails").innerHTML = "This is a mediocre laptop and costs 200kr";
  }else if(laptopSelection==3){
    document.getElementById("laptopselection").innerHTML = "You selected Nvydya Mountain";
    document.getElementById("laptopdetails").innerHTML = "This is a good laptop and costs 300kr";
  }else if(laptopSelection==4){
    document.getElementById("laptopselection").innerHTML = "You selected HB Elitepad";
    document.getElementById("laptopdetails").innerHTML = "This is an awesome laptop and costs 400kr";
  }
  document.getElementById("laptopmessage").innerHTML = "";
}

function buyNow() {
  if(bankBalance>100*laptopSelection){
    bankBalance=bankBalance-100*laptopSelection;
    document.getElementById("bankbalance").innerHTML = "Balance "+bankBalance+ "kr";
    document.getElementById("paybalance").innerHTML = "Pay "+payBalance+ "kr";
    document.getElementById("loanbalance").innerHTML = "Outstanding loan "+loanBalance+ "kr";
    document.getElementById("laptopmessage").innerHTML = "LAPTOP PURCHASE SUCCESSFUL!!";
  }else{
    document.getElementById("laptopmessage").innerHTML = "INSUFFICIENT BANK BALANCE FOR THE LAPTOP PURCHASE";
  }
}

